<?php

/**
 * @file
 * Rules integration for the Commerce discount products module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_discount_products_rules_condition_info() {
  $items = array();

  $items['commerce_discount_products_condition'] = array(
    'label' => t('Check discounted products'),
    'group' => t('Commerce Discount'),
    'parameter' => array(
      'commerce_discount' => array(
        'label' => t('Commerce discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line item'),
      ),
    ),
    'base' => 'commerce_discount_products_condition',
  );

  return $items;
}

/**
 * Rules condition: Check if discount can be applied.
 */
function commerce_discount_products_condition($discount_name, $line_item) {
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  $line_item_product = $line_item_wrapper->commerce_product->value();
  foreach($discount_wrapper->commerce_discount_products->value() as $product) {
    if ($product->product_id == $line_item_product->product_id)
      return TRUE;
  }
  return FALSE;
}

