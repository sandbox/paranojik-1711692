<?php

/**
 * @file
 * Views defaults for Commerce discount products module.
 */

/**
 * Implements hook_views_default_views_alter().
 *
 * Add products list to the discount overview.
 */
function commerce_discount_products_views_default_views_alter(&$views) {
  $products_field = _commerce_discount_products_views_get_fields();

  $view = &$views['commerce_discount_overview'];
  $handler = &$view->display['default']->handler;

  // In order to insert the product list in the middle of the fields array,
  // rebuild it while inserting the date fields after the 'rendered_entity'
  // field.
  foreach ($handler->display->display_options['fields'] as $field_id => $field) {
    $fields[$field_id] = $field;

    if ($field_id == 'rendered_entity') {
      $fields += $products_field;
    }
  }

  // Overwrite the original fields array.
  $handler->display->display_options['fields'] = $fields;
}

/**
 * Helper function to get the Views import.
 */
function _commerce_discount_products_views_get_fields() {
  $handler = new stdClass();
  $handler->display->display_options['fields']['commerce_discount_products']['id'] = 'commerce_discount_products';
  $handler->display->display_options['fields']['commerce_discount_products']['table'] = 'field_data_commerce_discount_products';
  $handler->display->display_options['fields']['commerce_discount_products']['field'] = 'commerce_discount_products';
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['trim'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['alter']['html'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['type'] = 'commerce_product_reference_title_link';
  $handler->display->display_options['fields']['commerce_discount_products']['settings'] = array(
    'show_quantity' => 0,
    'default_quantity' => '1',
    'combine' => 1,
    'line_item_type' => 0,
  );
  $handler->display->display_options['fields']['commerce_discount_products']['group_rows'] = 1;
  $handler->display->display_options['fields']['commerce_discount_products']['delta_offset'] = '0';
  $handler->display->display_options['fields']['commerce_discount_products']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['commerce_discount_products']['field_api_classes'] = 0;
  return $handler->display->display_options['fields'];
}
